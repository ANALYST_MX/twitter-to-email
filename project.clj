(defproject twitter-to-email "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [selmer "0.8.0"]
                 [twitter-api "0.7.8"]
                 [sonian/carica "1.1.0"]
                 [clj-http "1.0.1"]
                 [clj-time "0.9.0"]]
  :main twitter-to-email.core)
