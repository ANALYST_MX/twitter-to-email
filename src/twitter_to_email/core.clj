(ns twitter-to-email.core
  (:use [carica.core]
        [twitter.oauth]
        [twitter.callbacks]
        [twitter.callbacks.handlers]
        [twitter.api.restful]
        [selmer.filters]
        [selmer.parser :only [render-file set-resource-path!]]
        [clj-time.format :only [parse unparse formatter formatters]]
        [clj-http.client :only [post]])
  (:require [clj-time.core :as t])
  (:import
   (twitter.callbacks.protocols SyncSingleCallback)))

(set-resource-path! (clojure.java.io/resource "templates"))

(defn parse-date
  [format]
  (fn [date-string]
    (cond
      (keyword? format) (parse (formatters format) date-string)
      (string? format) (parse (formatter format) date-string))))

(defonce now (t/now))

(def ^:private creds (make-oauth-creds (config :twitter :consumer_key)
                                       (config :twitter :consumer_secret)
                                       (config :twitter :access_token)
                                       (config :twitter :access_token_secret)))

(def fixed-date (t/minus now (t/seconds (config :period))))

(def date (parse-date "E MMM dd HH:mm:ss Z YYYY"))

(def base-request {:count 100
                   :exclude-replies true
                   :include-rts false})

(def template (render-file "template.html" {}))

(def mail-domain (format "https://api.mailgun.net/v2/%s/messages" (config :mailgun :domain)))

(defn user-timeline [screen-name & {:as opts}]
  (statuses-user-timeline :callbacks (SyncSingleCallback. response-return-body
                                                          response-throw-error
                                                          exception-rethrow)
                          :oauth-creds creds
                          :params (merge (assoc base-request :screen-name screen-name) opts)))

(defn fetch-tweets
  [username]
  (map (fn [tweet]
         {:username (get-in tweet [:user :screen_name])
          :text (:text tweet)
          :id (:id tweet)
          :datetime (date (:created_at tweet))}) (user-timeline username)))

(defn created-later?
  [coll]
  (when-let [created (:datetime coll)]
    (t/after? created fixed-date)))

(defn filter-tweets
  [tweets]
  (filter created-later? tweets))

(defn tweets-group-by-account
  [category]
  {:name (name category) :accounts (vec (for [account (category (config :sources))
                                              :let [tweets (filter-tweets (fetch-tweets account))]]
                                          {:username account :tweets tweets}))})

(defn get-tweets []
  (for [category (keys (config :sources))]
    (tweets-group-by-account category)))

(defn render-mail []
  (render-file "template.html" {:categories (get-tweets)}))

(defn send-email []
  (post mail-domain
        {:basic-auth ["api" (config :mailgun :key)]
         :form-params
         {:from (format "Bot <postmaster@%s>" (config :mailgun :domain))
          :to (config :email)
          :subject (format "Digest for %s" (unparse (formatter "yyyy-MM-dd HH:mm") now))
          :html (render-mail)}}))

(defn -main []
  (send-email))
